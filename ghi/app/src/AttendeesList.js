import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';



function AttendeesList(props) {
    const [filterValue, setFilterValue] = useState("");

    function handleFilterChange(e) {
        setFilterValue(e.target.value);
    } 

    return (
        <div>
            <div className="form-floating mt-3">
                <input onChange={handleFilterChange} className="form-control"/>
                <label>Search By Name...</label>
            </div>
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Conference</th>
            </tr>
            </thead>
            <tbody>
            {props.attendees
            .filter((attendees) =>
            attendees.name.toLowerCase().includes(filterValue.toLowerCase())
            )
            .map(person => {
                return (
                <tr key={ person.href }>
                    <td>{ person.name }</td>
                    <td>{ person.conference }</td>
                </tr>
                );
            })}
            </tbody>
        </table>
        <Link to="./new">
            <button className="btn btn-primary">Create A New Attendee</button>
        </Link>
        </div>
    );
        }


export default AttendeesList;